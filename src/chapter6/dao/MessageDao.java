package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

	 public void insert(Connection connection, Message message) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("INSERT INTO messages ( ");
	            sql.append("    user_id, ");
	            sql.append("    text, ");
	            sql.append("    created_date, ");
	            sql.append("    updated_date ");
	            sql.append(") VALUES ( ");
	            sql.append("    ?, ");                  // user_id
	            sql.append("    ?, ");                  // text
	            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
	            sql.append("    CURRENT_TIMESTAMP ");   // updated_date
	            sql.append(")");

	            ps = connection.prepareStatement(sql.toString());

	            ps.setInt(1, message.getUserId());
	            ps.setString(2, message.getText());

	            ps.executeUpdate();
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

    public void delete(Connection connection, String messageId) {
    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("DELETE FROM messages WHERE id = ?;");

    		ps = connection.prepareStatement(sql.toString());

    		ps.setString(1, messageId);

    		ps.executeUpdate();

    	  } catch (SQLException e) {
              throw new SQLRuntimeException(e);
          } finally {
              close(ps);
          }
    }

    	//投稿の取得文

    public Message select(Connection connection, int id) {
    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("SELECT ");
			sql.append("    messages.id as id, ");
			sql.append("    messages.text as text, ");
			sql.append("    messages.user_id as user_id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<Message> messages = toMessages(rs);
			if(messages.isEmpty()) {
				return null;
			} else if (2 <= messages.size()) {
				throw new IllegalStateException("投稿が重複しています");
			} else {
				return messages.get(0);
			}

    	  } catch (SQLException e) {
              throw new SQLRuntimeException(e);
          } finally {
              close(ps);
          }
    }


    private List<Message> toMessages(ResultSet rs) throws SQLException {

		List<Message> messages = new ArrayList<Message>();
		try {
			while (rs.next()) {
				Message message = new Message();
				message.setId(rs.getInt("id"));
				message.setText(rs.getString("text"));
				message.setUserId(rs.getInt("user_id"));
				message.setCreatedDate(rs.getTimestamp("created_date"));


				messages.add(message);
			}
			return messages;
		} finally {
			close(rs);
		}
	}


    public void update(Connection connection, Message message) {
    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("UPDATE messages SET");
    		sql.append("	text = ?, ");
    		sql.append("	updated_date = CURRENT_TIMESTAMP ");
    		sql.append("WHERE id = ?");

    		ps = connection.prepareStatement(sql.toString());

    		ps.setString(1,message.getText());
    		ps.setInt(2,message.getId());

    		ps.executeUpdate();

    	  } catch (SQLException e) {
              throw new SQLRuntimeException(e);
          } finally {
              close(ps);
          }
    }
}